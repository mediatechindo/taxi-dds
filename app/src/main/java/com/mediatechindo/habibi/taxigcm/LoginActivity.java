package com.mediatechindo.habibi.taxigcm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;

/**
 * Created by habibi on 12/22/14.
 */
public class LoginActivity extends Activity {
    AlertDialogManager alert = new AlertDialogManager();

    // Internet detector
    ConnectionDetector cd;

    EditText emailField;
    EditText passwordField;

    Button loginButton;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        cd = new ConnectionDetector(getApplicationContext());

        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(LoginActivity.this,
                    "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }

        emailField = (EditText) findViewById(R.id.emailField);
        passwordField = (EditText) findViewById(R.id.passwordField);
        loginButton = (Button) findViewById(R.id.loginButton);

        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String email = emailField.getText().toString();
                String password = passwordField.getText().toString();
                if(email.trim().length() > 0 && password.trim().length() > 0){
                    // Launch Main Activity
                    //Intent i = new Intent(getApplicationContext(), MainActivity.class);


                } else {
                    alert.showAlertDialog(LoginActivity.this, "Login Error!", "Please enter your details", false);
                }
            }
        });


    }
}
