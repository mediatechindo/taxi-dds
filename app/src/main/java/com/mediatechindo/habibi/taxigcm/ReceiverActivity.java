package com.mediatechindo.habibi.taxigcm;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;

import org.json.JSONObject;

import static com.mediatechindo.habibi.taxigcm.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.mediatechindo.habibi.taxigcm.CommonUtilities.EXTRA_MESSAGE;
import static com.mediatechindo.habibi.taxigcm.CommonUtilities.EXTRA_MESSAGE_TYPE;

/**
 * Created by habibi on 12/30/14.
 */
public class ReceiverActivity extends Activity {
    LinearLayout mainLayout;
    TextView txtFromLabel;
    TextView txtToLabel;
    TextView txtNameLabel;
    TextView txtPhoneLabel;
    TextView txtCommentLabel;
    Button applyButton;
    Button declineButton;

    public static String notificationMessageType=null;
    public JSONObject notificationMessageJSON;

    AsyncTask<Void, Void, Void> mApplyOrderTask;
    AsyncTask<Void, Void, Void> mCancelOrderTask;

    public String orderMessage;
    public static String loggedUserData=null;
    public String driverid=null;
    public String parentid=null;

    public String orderId="";

    public ProgressDialog muhDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);

        setTitle("New Order!");

        mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
        txtFromLabel = (TextView) findViewById(R.id.txtFromLabel);
        txtToLabel = (TextView) findViewById(R.id.txtToLabel);
        txtNameLabel = (TextView) findViewById(R.id.txtNameLabel);
        txtPhoneLabel = (TextView) findViewById(R.id.txtPhoneLabel);
        txtCommentLabel = (TextView) findViewById(R.id.txtCommentLabel);

        applyButton = (Button) findViewById(R.id.applyButton);
        declineButton = (Button) findViewById(R.id.declineButton);

        Intent i = getIntent();
        orderMessage = i.getStringExtra("orderMessage");
        loggedUserData = i.getStringExtra("loggedUserData");
        if (orderMessage.length()>0) {
            parseOrder(orderMessage);
        }

        try {
            final JSONObject loggedUserDataJSON = new JSONObject(loggedUserData);
            parentid = loggedUserDataJSON.getString("user_parent_id");
            driverid = loggedUserDataJSON.getString("id");

            final Context context = this;
            mApplyOrderTask = new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... params) {
                    // Register on our server
                    // On server creates a new user
                    ServerUtilities.applyOrder(context, orderId, driverid, parentid);
                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    mApplyOrderTask = null;
                }
            };

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        final Context context = this;
        final String regId = GCMRegistrar.getRegistrationId(this);
        mCancelOrderTask = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                // Register on our server
                // On server creates a new user
                ServerUtilities.drivercanceled(context, orderId, regId);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                mCancelOrderTask = null;

                muhDialog.dismiss();

                MainActivity.notificationMessage=null;
                MainActivity.notificationMessageType=null;
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
            }
        };

    }

    public void parseOrder(String newMessage) {
        //Respond is JSON, parse it
        try {
            final JSONObject reader = new JSONObject(newMessage);
            notificationMessageJSON = reader;
            orderId = reader.getString("id");

            applyButton = (Button) findViewById(R.id.applyButton);
            declineButton = (Button) findViewById(R.id.declineButton);

            //Toast.makeText(getApplicationContext(), "New Message: " + comt, Toast.LENGTH_LONG).show();


            if (notificationMessageType!=null) {
                if (notificationMessageType.equals("order")) {
                    Toast.makeText(getApplicationContext(), "New Order Available!", Toast.LENGTH_LONG).show();
                } else {
                    return;
                }
            } else {
                Toast.makeText(getApplicationContext(), "New Order Available!", Toast.LENGTH_LONG).show();
            }

            mainLayout.setVisibility(LinearLayout.VISIBLE);
            txtFromLabel.setText(reader.getString("from"));
            txtToLabel.setText(reader.getString("to"));
            txtNameLabel.setText(reader.getString("name"));
            txtPhoneLabel.setText(reader.getString("phone"));
            txtCommentLabel.setText(reader.getString("comment"));




            applyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    alertOrderMessage("apply",orderId);
                }
            });

            declineButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    alertOrderMessage("decline",orderId);
                }
            });

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    public void cancelAndBackToMain() {
        //remove
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("taxigcmappcurrentorder").commit();

        muhDialog = ProgressDialog.show(ReceiverActivity.this, "","Menghapus order...", true);

        mCancelOrderTask.execute(null, null, null);


    }

    public void alertOrderMessage(final String param, final String orderId) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE: // Yes button clicked
                        if (param.equals("apply")) {
                            Intent i = new Intent(getApplicationContext(), ExecuteActivity.class);
                            i.putExtra("orderMessage", orderMessage);
                            startActivity(i);
                            finish();

                            mApplyOrderTask.execute(null, null, null);

                        } else if (param.equals("decline")) {
                            cancelAndBackToMain();
                        }
                        //Toast.makeText(MainActivity.this, "Yes Clicked "+orderId, Toast.LENGTH_LONG).show();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE: // No button clicked // do nothing
                        if (param.equals("apply")) {

                        } else if (param.equals("decline")) {

                        }
                        //Toast.makeText(MainActivity.this, "No Clicked "+orderId, Toast.LENGTH_LONG).show();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure?")
                .setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener)
                .show();
    }
}
