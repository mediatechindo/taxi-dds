package com.mediatechindo.habibi.taxigcm;

import android.app.AlertDialog;
import android.app.Service;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.widget.Toast;
import android.content.BroadcastReceiver;

import org.json.JSONObject;

/**
 * Created by habibi on 12/16/14.
 */

public class LocationService extends Service {

    private LoggerLoadTask mTask;
    private String mPulseUrl;
    private AlarmManager alarms;
    private PendingIntent alarmIntent;

    private Boolean flag = false;
    private LocationManager locationManager=null;
    private LocationListener locationListener=null;
    public static String deviceLat;
    public static String deviceLng;

    ConnectionDetector cd;
    BroadcastReceiver br;
    String regId;
    String loggedUserData=null;
    JSONObject loggedUserDataJSON;
    String userParentId=null;

    @Override
    public void onCreate() {
        super.onCreate();

        alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intentOnAlarm = new Intent(CommonUtilities.ACTION_PULSE_SERVER_ALARM); // LaunchReceiver.ACTION_PULSE_SERVER_ALARM);
        alarmIntent = PendingIntent.getBroadcast(this, 0, intentOnAlarm, 0);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        /*flag = displayGpsStatus();
        if (flag) {
            locationListener = new MyLocationListener();
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10,locationListener);

        } else {
            alertbox("Gps Status!!", "Your GPS is: OFF");
        }*/

        locationListener = new MyLocationListener();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10,locationListener);

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context c, Intent i) {
                //Toast.makeText(c, "Rise and Shine!", Toast.LENGTH_LONG).show();
                executeLogger();
            }
        };


    }

    /*----------Method to create an AlertBox ------------- */
    protected void alertbox(String title, String mymessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your Device's GPS is Disable")
                .setCancelable(false)
                .setTitle("** Gps Status **")
                .setPositiveButton("Gps On",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // finish the current activity
                                // AlertBoxAdvance.this.finish();
                                Intent myIntent = new Intent(
                                        Settings.ACTION_SECURITY_SETTINGS);
                                startActivity(myIntent);
                                dialog.cancel();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // cancel the dialog box
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*----Method to Check GPS is enable or disable ----- */
    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = getBaseContext()
                .getContentResolver();
        boolean gpsStatus = Settings.Secure
                .isLocationProviderEnabled(contentResolver,
                        LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;

        } else {
            return false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("Location Service", "Received start id " + startId + ": " + intent);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.

        regId = intent.getStringExtra("regId");
        loggedUserData = intent.getStringExtra("loggedUserData");

        try {
            loggedUserDataJSON = new JSONObject(loggedUserData);
            userParentId = loggedUserDataJSON.getString("user_parent_id");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Toast.makeText(getBaseContext(), "Location Service Started" + regId,Toast.LENGTH_SHORT).show();

        registerReceiver(br, new IntentFilter(CommonUtilities.ACTION_PULSE_SERVER_ALARM) );
        executeLogger();

        return START_STICKY;
        //return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void executeLogger() {
        if (mTask != null && mTask.getStatus() != LoggerLoadTask.Status.FINISHED) {
            Log.e("mTask", "> " + "is wrong!1!");
            return;
        }

        /*if (deviceLat==null) {
            Log.e("deviceLat", "> " + "is NULL");
            return;
        }*/

        mTask = (LoggerLoadTask) new LoggerLoadTask().execute();
    }

    private class LoggerLoadTask extends AsyncTask<Void, Void, Void> {

        // TODO: create two base service urls, one for debugging and one for live.
        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                // if we have no data connection, no point in proceeding.
                cd = new ConnectionDetector(getApplicationContext());

                // Check if Internet present
                if (!cd.isConnectingToInternet()) {
                    return null;
                }

                ServerUtilities.updatelatLng(getApplicationContext(), deviceLat, deviceLng, regId, userParentId);

                // / grab and log data
            } catch (Exception e) {
                Log.e("Exception", "> " + e.getMessage());
            } finally {
                long timeToAlarm = SystemClock.elapsedRealtime() + (CommonUtilities.GPS_DELAY_UPDATE * 1000);
                alarms.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, timeToAlarm, alarmIntent);
            }
            return null;
        }
    }

    /*----------Listener class to get coordinates ------------- */
    private class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location loc) {
            /*Toast.makeText(getBaseContext(), "Location changed : Lat: " +
                            loc.getLatitude() + " Lng: " + loc.getLongitude(),
                    Toast.LENGTH_SHORT).show();*/
            String latitude = "Latitude: " +loc.getLatitude();
            String longitude = "Longitude: " +loc.getLongitude();

            deviceLat = String.valueOf(loc.getLatitude());
            deviceLng = String.valueOf(loc.getLongitude());

            //Log.e("longitude", "> " + longitude);
            //Log.e("latitude", "> " + latitude);
        }

        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onStatusChanged(String provider,
                                    int status, Bundle extras) {
            // TODO Auto-generated method stub
        }
    }
}