package com.mediatechindo.habibi.taxigcm;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by habibi on 12/24/14.
 */
public class GPSListener implements LocationListener {

    double deviceLat;
    double deviceLng;

    @Override
    public void onLocationChanged(Location loc) {
            /*Toast.makeText(getBaseContext(), "Location changed : Lat: " +
                            loc.getLatitude() + " Lng: " + loc.getLongitude(),
                    Toast.LENGTH_SHORT).show();*/
        String latitude = "Latitude: " +loc.getLatitude();
        String longitude = "Longitude: " +loc.getLongitude();

        deviceLat = loc.getLatitude();
        deviceLng = loc.getLongitude();

        Log.e("GPS longitude", "> " + longitude);
        Log.e("GPS latitude", "> " + latitude);
    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onStatusChanged(String provider,
                                int status, Bundle extras) {
        // TODO Auto-generated method stub
    }
}
