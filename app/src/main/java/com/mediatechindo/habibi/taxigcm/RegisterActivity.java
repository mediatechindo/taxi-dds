package com.mediatechindo.habibi.taxigcm;

/**
 * Created by habibi on 12/11/14.
 */
import static com.mediatechindo.habibi.taxigcm.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.mediatechindo.habibi.taxigcm.CommonUtilities.EXTRA_MESSAGE;
import static com.mediatechindo.habibi.taxigcm.CommonUtilities.EXTRA_MESSAGE_TYPE;
import static com.mediatechindo.habibi.taxigcm.CommonUtilities.SENDER_ID;
//import com.mediatechindo.habibi.taxigcm.CommonUtilities.SERVER_URL;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gcm.GCMRegistrar;
import org.json.JSONObject;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class RegisterActivity extends Activity {
    // alert dialog manager
    AlertDialogManager alert = new AlertDialogManager();

    // Internet detector
    ConnectionDetector cd;
    ProgressDialog progressDialog;
    AsyncTask<Void, Void, Void> mRegisterTask;

    EditText txtPhone;
    Button btnRegister;

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Check if Internet present
        if (!isNetworkConnected()) {
            alert.showAlertDialog(RegisterActivity.this,
                    "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            return;
        }

        // Check if GCM configuration is set
        if (CommonUtilities.SERVER_URL == null || SENDER_ID == null || CommonUtilities.SERVER_URL.length() == 0
                || SENDER_ID.length() == 0) {
            alert.showAlertDialog(RegisterActivity.this, "Configuration Error!",
                    "Please set your Server URL and GCM Sender ID", false);
            return;
        }

        txtPhone = (EditText) findViewById(R.id.txtPhone);
        btnRegister = (Button) findViewById(R.id.btnRegister);

        //developer mode
        txtPhone.setText("085607019124");

        //REGISTER TO RECEIVE NOTIFICATION BROADCAST!!
        registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_MESSAGE_ACTION));

		/*
		 * Click event on Register button
		 * */
        btnRegister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Read EditText dat
                final String phone = txtPhone.getText().toString();

                // Check if user filled the form
                if (phone.trim().length() > 0) {
                    progressDialog = ProgressDialog.show(RegisterActivity.this, "", "Contacting Server...", true);

                    final Context context = getApplicationContext();
                    mRegisterTask = new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            ServerUtilities.checkPhone(context, phone);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void result) {
                            mRegisterTask = null;
                        }

                    };
                    mRegisterTask.execute(null, null, null);
                } else {
                    // user doen't filled that data
                    // ask him to fill the form
                    alert.showAlertDialog(RegisterActivity.this, "Registration Error!", "Please enter your details", false);
                }
            }
        });



        // Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(this);

        // Get GCM registration id
        final String regId = GCMRegistrar.getRegistrationId(this);
        if (!regId.equals("")) {
            Toast.makeText(getApplicationContext(), "Already registered with GCM", Toast.LENGTH_LONG).show();

            Intent i = new Intent(getApplicationContext(), MainActivity.class);

            //Retrieve SharedPreferences first
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String currentLoggedUserData = preferences.getString("taxigcmcurrentloggeduserdata","");
            if (currentLoggedUserData.length()>0) {
                String phone=null;
                try {
                    JSONObject loggedUserDataJSON = new JSONObject(currentLoggedUserData);
                    phone = loggedUserDataJSON.getString("phone");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                i.putExtra("phone", phone);
                i.putExtra("loggedUserData", currentLoggedUserData);
                startActivity(i);
                finish();
            }


        }
    }

    protected void alertBoxForShit(final Context ctx, String title, String mymessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(mymessage)
                .setCancelable(false)
                .setTitle(title)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onDestroy() {
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        try {
            unregisterReceiver(mHandleMessageReceiver);
            GCMRegistrar.onDestroy(this);
        } catch (Exception e) {
            Log.e("UnRegister Receiver Error", "> " + e.getMessage());
        }
        super.onDestroy();
    }

    /**
     * Receiving push messages
     * */
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            String messageType = intent.getExtras().getString(EXTRA_MESSAGE_TYPE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());

            if (messageType.equals("registerbyphone")) {

                //Save the current loggedUserData to Shared Preferences
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(RegisterActivity.this);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("taxigcmcurrentloggeduserdata",newMessage);
                editor.apply();

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("phone", txtPhone.getText().toString());
                i.putExtra("loggedUserData", newMessage);
                startActivity(i);
                finish();

                progressDialog.dismiss();

            } else if (messageType.equals("registerbyphoneerror")) {
                progressDialog.dismiss();
                alertBoxForShit(getApplicationContext(), "Taxi", newMessage);
            }



            // Releasing wake lock
            WakeLocker.release();
        }
    };

}
