package com.mediatechindo.habibi.taxigcm;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import android.util.Log;
import android.location.Location;

import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;

import android.os.AsyncTask;
import java.util.List;
import org.json.JSONObject;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.RadioGroup.LayoutParams;

import static com.mediatechindo.habibi.taxigcm.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.mediatechindo.habibi.taxigcm.CommonUtilities.EXTRA_MESSAGE;
import static com.mediatechindo.habibi.taxigcm.CommonUtilities.EXTRA_MESSAGE_TYPE;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by habibi on 12/12/14.
 */
public class ExecuteActivity extends Activity {
    Button toButtonLabel;
    Button fromButtonLabel;
    LinearLayout layoutBottom;

    public String orderMessage;
    public JSONObject orderMessageJSON;

    public static String notificationMessage=null;
    public static String notificationMessageType=null;

    // Google Map
    private GoogleMap googleMap;

    private Boolean flag = false;
    private LocationManager locationManager=null;
    private LocationListener locationListener=null;

    public double latitude;
    public double longitude;

    public double fromLat;
    public double fromLng;
    public double toLat;
    public double toLng;
    public String orderId="";
    public String driverId="";

    private HashMap<Marker, MyMarker> mMarkersHashMap;
    private ArrayList<MyMarker> mMyMarkersArray = new ArrayList<MyMarker>();

    AsyncTask<Void, Void, Void> mCancelOrderTask;

    public ProgressDialog muhDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_execute);

        toButtonLabel = (Button) findViewById(R.id.toButtonLabel);
        fromButtonLabel = (Button) findViewById(R.id.fromButtonLabel);
        layoutBottom = (LinearLayout) findViewById(R.id.layoutBottom);

        Intent intent = getIntent();
        orderMessage = intent.getStringExtra("orderMessage");



        try {
            orderMessageJSON = new JSONObject(orderMessage);
            fromLat = Double.parseDouble(orderMessageJSON.getString("from_lat"));
            fromLng = Double.parseDouble(orderMessageJSON.getString("from_lng"));
            toLat = Double.parseDouble(orderMessageJSON.getString("to_lat"));
            toLng = Double.parseDouble(orderMessageJSON.getString("to_lng"));
            toButtonLabel.setText(orderMessageJSON.getString("to"));
            fromButtonLabel.setText(orderMessageJSON.getString("from"));

            orderId = orderMessageJSON.getString("id");
            driverId = orderMessageJSON.getString("driver_id");

            if (orderMessageJSON.getString("status").equals("OK")) {
                setTitle("Order Started!");
                makeBottomArriveLayout();
            } else {
                setTitle("Menunggu persetujuan dari Server...");
                makeBottomCancelLayout();
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        registerReceiver(mHandleMessageReceiverExecuteActivity, new IntentFilter(DISPLAY_MESSAGE_ACTION));

        toButtonLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

            }
        });

        fromButtonLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

            }
        });

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        flag = displayGpsStatus();
        if (flag) {
            //locationListener = new GPSListener();
            //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10,locationListener);

            locationManager = (LocationManager)this.getSystemService(LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                latitude=location.getLatitude();
                longitude=location.getLongitude();
                Log.e("###### GPS longitude", "> " + longitude);
                Log.e("###### GPS latitude", "> " + latitude);
            }

        } else {
            alertbox("Gps Status!!", "Your GPS is: OFF");
        }


        // Initialize the HashMap for Markers and MyMarker object
        mMarkersHashMap = new HashMap<Marker, MyMarker>();

        /*fromLat=-7.773686;
        fromLng=110.368773;
        toLat=-7.801675;
        toLng=110.365000;*/

        //mMyMarkersArray.add(new MyMarker("LocA", "icon1", -7.773686, 110.368773)); //Hotel Tentrem
        //mMyMarkersArray.add(new MyMarker("LocB", "icon1", -7.772025, 110.377380)); //MM UGM
        //mMyMarkersArray.add(new MyMarker("LocB", "icon1", -7.801675, 110.365000)); // Kantor Pos 0 KM

        mMyMarkersArray.add(new MyMarker("LocA", "icon1", fromLat, fromLng));
        mMyMarkersArray.add(new MyMarker("LocA", "icon1", toLat, toLng));

        initializeMap();
        plotMarkers(mMyMarkersArray);

        String url = getMapsApiDirectionsUrl(new LatLng(fromLat,fromLng), new LatLng(toLat, toLng));
        ReadTask downloadTask = new ReadTask();
        downloadTask.execute(url);

        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                new LatLng(latitude, longitude)).zoom(14).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


        final Context context = this;
        final String regId = GCMRegistrar.getRegistrationId(this);
        mCancelOrderTask = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                // Register on our server
                // On server creates a new user
                ServerUtilities.drivercanceled(context, orderId, regId);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                mCancelOrderTask = null;

                muhDialog.dismiss();

                MainActivity.notificationMessage=null;
                MainActivity.notificationMessageType=null;
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
            }
        };

    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(mHandleMessageReceiverExecuteActivity);
            GCMRegistrar.onDestroy(this);
        } catch (Exception e) {
            Log.e("UnRegister Receiver Error", "> " + e.getMessage());
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }

    public void cancelButtonDidPressed() {
        //remove
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("taxigcmappcurrentorder").commit();

        //muhDialog = ProgressDialog.show(ExecuteActivity.this, "","Menghapus order...", true);
        //mCancelOrderTask.execute(null, null, null);


    }

    public void makeBottomCancelLayout() {
        //Cancel and Arrived
        layoutBottom.removeAllViews();
        Button btnTag = new Button(this);
        btnTag.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1.0f));
        btnTag.setText("Cancel");
        btnTag.setBackgroundColor(Color.BLACK);
        btnTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                cancelButtonDidPressed();
            }
        });
        btnTag.setId(0);
        layoutBottom.addView(btnTag);
    }

    public void makeBottomFinishLayout() {
        //Cancel and Arrived
        layoutBottom.removeAllViews();
        for (int j = 0; j < 2; j++) {
            Button btnTag = new Button(this);
            btnTag.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1.0f));
            if (j==0) {
                btnTag.setText("Cancel");
                btnTag.setBackgroundColor(Color.BLACK);
                btnTag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        cancelButtonDidPressed();
                    }
                });
            } else {
                btnTag.setText("Finish");
                btnTag.setBackgroundColor(Color.RED);
                btnTag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {

                    }
                });
            }
            btnTag.setId(j);
            layoutBottom.addView(btnTag);
        }
    }

    public void makeBottomStartARideLayout() {
        //Cancel and Arrived
        layoutBottom.removeAllViews();
        for (int j = 0; j < 3; j++) {
            Button btnTag = new Button(this);
            btnTag.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1.0f));
            if (j==0) {
                btnTag.setText("Cancel");
                btnTag.setBackgroundColor(Color.BLACK);
                btnTag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        cancelButtonDidPressed();
                    }
                });
            } else if (j==1) {
                btnTag.setText("No Contact");
                btnTag.setBackgroundColor(Color.BLACK);
                btnTag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {

                    }
                });
            } else {
                btnTag.setText("Start a Ride");
                btnTag.setBackgroundColor(Color.RED);
                btnTag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {

                    }
                });
            }
            btnTag.setId(j);

            layoutBottom.addView(btnTag);
        }
    }

    public void makeBottomArriveLayout() {
        //Cancel and Arrived
        layoutBottom.removeAllViews();
        for (int j = 0; j < 2; j++) {
            Button btnTag = new Button(this);
            btnTag.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1.0f));
            if (j==0) {
                btnTag.setText("Cancel");
                btnTag.setBackgroundColor(Color.BLACK);
                btnTag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        cancelButtonDidPressed();
                    }
                });
            } else {
                btnTag.setText("Arrived");
                btnTag.setBackgroundColor(Color.RED);
                btnTag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        makeBottomStartARideLayout();
                    }
                });
            }
            btnTag.setId(j);

            layoutBottom.addView(btnTag);
        }
    }

    public void proceedOrder(String newMessage) {
        if (notificationMessageType.equals("order")) {
            return;
        } else if (notificationMessageType.equals("approval")) {
            Toast.makeText(getApplicationContext(), "Driver Approval!", Toast.LENGTH_LONG).show();
        }

        if (newMessage.equals("NULL")) { //rejected
            new AlertDialog.Builder(this)
                    .setTitle("Taxi")
                    .setMessage("You have been rejected as a driver for this order.")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            cancelButtonDidPressed();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            Toast.makeText(getApplicationContext(), "Driver request approved!", Toast.LENGTH_LONG).show();
            setTitle("Order Started!");
            makeBottomArriveLayout();

            //Save the current order to Shared Preferences
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("taxigcmappcurrentorder",newMessage);
            editor.apply();

            //remove
            //editor.remove("taxigcmappcurrentorder").commit();
        }

        try {
            final JSONObject reader = new JSONObject(newMessage);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Receiving push messages
     * */
    private final BroadcastReceiver mHandleMessageReceiverExecuteActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            String messageType = intent.getExtras().getString(EXTRA_MESSAGE_TYPE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());

            notificationMessage = newMessage;
            notificationMessageType = messageType;
            proceedOrder(newMessage);

            // Releasing wake lock
            WakeLocker.release();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        initializeMap();
    }

    /*----------Method to create an AlertBox ------------- */
    protected void alertbox(String title, String mymessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your Device's GPS is Disable")
                .setCancelable(false)
                .setTitle("** Gps Status **")
                .setPositiveButton("Gps On",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // finish the current activity
                                // AlertBoxAdvance.this.finish();
                                Intent myIntent = new Intent(
                                        Settings.ACTION_SECURITY_SETTINGS);
                                startActivity(myIntent);
                                dialog.cancel();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // cancel the dialog box
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*----Method to Check GPS is enable or disable ----- */
    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = getBaseContext()
                .getContentResolver();
        boolean gpsStatus = Settings.Secure
                .isLocationProviderEnabled(contentResolver,
                        LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;

        } else {
            return false;
        }
    }

    /**
     * function to load map. If map is not created it will create it for you
     * */
    private void initializeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            } else {

            }
        }
        googleMap.setMyLocationEnabled(true);


    }

    public void createMarker(double lat, double lng) {
        //-7.772025, 110.377380

        // create marker
        MarkerOptions marker = new MarkerOptions().position(new LatLng(lat, lng)).title("Hello Maps");
        // Changing marker icon
        //marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.my_marker_icon)));

        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
        // adding marker
        googleMap.addMarker(marker);
    }

    private void plotMarkers(ArrayList<MyMarker> markers){
        if(markers.size() > 0)
        {
            //LatLngBounds.Builder builder = new LatLngBounds.Builder();
            int idx=0;
            for (MyMarker myMarker : markers)
            {
                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(myMarker.getmLatitude(), myMarker.getmLongitude()));

                if (idx==0) markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
                //else markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_point_b));

                Marker currentMarker = googleMap.addMarker(markerOption);
                mMarkersHashMap.put(currentMarker, myMarker);
                idx++;
                //builder.include(currentMarker.getPosition());
            }
            /*LatLngBounds bounds = builder.build();
            int padding = 0; // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            googleMap.moveCamera(cu);
            googleMap.animateCamera(cu);*/
        }

    }

    //https://maps.googleapis.com/maps/api/directions/json?waypoints=optimize:true|-7.773686,110.368773|-7.801759,110.365488|
    private String getMapsApiDirectionsUrl(LatLng from, LatLng to ) {
        String waypoints = "waypoints=optimize:true|"
                + from.latitude + "," + from.longitude
                + "|" + to.latitude + ","
                + to.longitude;

        String sensor = "sensor=false";
        String params = waypoints + "&" + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + params;
        return url;
    }

    private class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                ServerUtilities http = new ServerUtilities();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }
    }

    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;

            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<LatLng>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                polyLineOptions.addAll(points);
                polyLineOptions.width(4);
                //Color.argb(1, 192, 57, 43)
                //polyLineOptions.color(Color.BLUE);
                polyLineOptions.color(Color.rgb(42,94,192));
            }

            googleMap.addPolyline(polyLineOptions);
        }
    }


}
