package com.mediatechindo.habibi.taxigcm;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.Button;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.app.ProgressDialog;
import java.util.Timer;
import java.util.TimerTask;
import android.location.LocationManager;
import android.location.LocationListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.os.Handler;
import android.provider.Settings;
import android.content.ContentResolver;
import android.location.Location;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.mediatechindo.habibi.taxigcm.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.mediatechindo.habibi.taxigcm.CommonUtilities.EXTRA_MESSAGE;
import static com.mediatechindo.habibi.taxigcm.CommonUtilities.EXTRA_MESSAGE_TYPE;
import static com.mediatechindo.habibi.taxigcm.CommonUtilities.SENDER_ID;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import com.google.android.gcm.GCMRegistrar;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class MainActivity extends Activity {
    // label to display gcm messages
    TextView lblMessage;
    LinearLayout mainLayout;

    private static Boolean gpsFlag = false;

    public static ProgressDialog progressDialog=null;
    AsyncTask<Void, Void, Void> mRegisterTask;
    AsyncTask<Void, Void, Void> mApplyOrderTask;
    AsyncTask<Void, Void, Void> mUpdateRegIdTask;
    AlertDialogManager alert = new AlertDialogManager();
    ConnectionDetector cd;

    public static String phone;
    public static String notificationMessage=null;
    public static String notificationMessageType=null;
    public static String loggedUserData=null;

    Timer timer;
    TimerTask timerTask;

    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
        mainLayout.setVisibility(LinearLayout.GONE); //Hide

        cd = new ConnectionDetector(getApplicationContext());

        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            alert.showAlertDialog(MainActivity.this,
                    "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            return;
        }

        Intent i = getIntent();
        phone = i.getStringExtra("phone");
        loggedUserData = i.getStringExtra("loggedUserData");

        // Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(this);

        // Make sure the manifest was properly set - comment out this line
        // while developing the app, then uncomment it when it's ready.
        GCMRegistrar.checkManifest(this);

        final String regId = GCMRegistrar.getRegistrationId(this);


        //Check GPS thingie
        gpsFlag = displayGpsStatus();
        if (!gpsFlag) {
            alertbox("Gps Status!!", "Your GPS is: OFF");
            return;
        }

        //REGISTER TO RECEIVE NOTIFICATION BROADCAST!!
        registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_MESSAGE_ACTION));

        //Retrieve SharedPreferences first
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String orderDataFromPref = preferences.getString("taxigcmappcurrentorder","");
        if (orderDataFromPref.length()>0) {
            Intent intent = new Intent(getApplicationContext(), ExecuteActivity.class);
            intent.putExtra("orderMessage", orderDataFromPref);
            startActivity(intent);
            finish();
        }

        // Check if regid already presents
        if (regId.equals("")) {
            GCMRegistrar.register(this, SENDER_ID);
            progressDialog =ProgressDialog.show(MainActivity.this, "","Contacting GCM Server...", true);
        } else {
            // Device is already registered on GCM
            if (GCMRegistrar.isRegisteredOnServer(this)) {
                if (gpsFlag) {
                    startLocationService(getApplicationContext(), regId);
                }
            } else {
                final Context context = this;
                mRegisterTask = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        ServerUtilities.checkPhone(context, phone);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        mRegisterTask = null;
                    }

                };
                mRegisterTask.execute(null, null, null);
            }
        }

        if (notificationMessage!=null) {
            Intent intent = new Intent(getApplicationContext(), ReceiverActivity.class);
            intent.putExtra("orderMessage", notificationMessage);
            startActivity(intent);
            finish();
        }

    }

    /*----------Method to create an AlertBox ------------- */
    protected void alertbox(String title, String mymessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your Device's GPS is Disable")
                .setCancelable(false)
                .setTitle("** Gps Status **")
                .setPositiveButton("Gps On",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // finish the current activity
                                // AlertBoxAdvance.this.finish();
                                Intent myIntent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
                                startActivity(myIntent);
                                dialog.cancel();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // cancel the dialog box
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*----Method to Check GPS is enable or disable ----- */
    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = getBaseContext().getContentResolver();
        boolean gpsStatus = Settings.Secure.isLocationProviderEnabled(contentResolver,LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;
        } else {
            return false;
        }
    }

    public static void startLocationService(Context context, String regId) {
        Intent serviceIntent = new Intent(context,LocationService.class);
        serviceIntent.putExtra("regId",regId);
        serviceIntent.putExtra("loggedUserData",loggedUserData);
        context.startService(serviceIntent);
    }

    public static void stopProgressDialog() {
        progressDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //onResume we start our timer so it can start when the app comes from the background
        //registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_MESSAGE_ACTION));
    }

    protected void alertBoxForShit(final Context ctx, String title, String mymessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(mymessage)
                .setCancelable(false)
                .setTitle(title)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent i = new Intent(ctx, RegisterActivity.class);
                                startActivity(i);
                                finish();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Receiving push messages
     * */
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            String messageType = intent.getExtras().getString(EXTRA_MESSAGE_TYPE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());

            if (messageType.equals("registerbyphone")) {
                loggedUserData = newMessage;
                GCMRegistrar.register(MainActivity.this, SENDER_ID);
                progressDialog.dismiss();
            } else if (messageType.equals("registerbyphoneerror")) {
                alertBoxForShit(getApplicationContext(), "Taxi",newMessage);
                progressDialog.dismiss();
            } else if (messageType.equals("order")){
                notificationMessage = newMessage;
                notificationMessageType = messageType;
                progressDialog.dismiss();

                Intent i = new Intent(getApplicationContext(), ReceiverActivity.class);
                i.putExtra("orderMessage", notificationMessage);
                i.putExtra("loggedUserData", loggedUserData);
                startActivity(i);
                finish();

            } else if (messageType.equals("gcmregistration")) {
                progressDialog.dismiss();
                progressDialog =ProgressDialog.show(MainActivity.this, "","Updating data...", true);
                GCMRegistrar.setRegisteredOnServer(context, true);
                final String regId = GCMRegistrar.getRegistrationId(MainActivity.this);
                mUpdateRegIdTask = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        ServerUtilities.updateRegId(context, phone, regId);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        mUpdateRegIdTask = null;
                    }

                };
                mUpdateRegIdTask.execute(null, null, null);
            } else if (messageType.equals("updateregid")) {
                progressDialog.dismiss();
            }

            // Releasing wake lock
            WakeLocker.release();
        }
    };

    @Override
    protected void onDestroy() {
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }

        if (mUpdateRegIdTask != null) {
            mUpdateRegIdTask.cancel(true);
        }
        try {
            unregisterReceiver(mHandleMessageReceiver);
            GCMRegistrar.onDestroy(this);
        } catch (Exception e) {
            Log.e("UnRegister Receiver Error", "> " + e.getMessage());
        }
        super.onDestroy();
    }

    public void launchRingDialog() {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(MainActivity.this, "Please wait ...",	"Obtaining approval...", true);
        ringProgressDialog.setCancelable(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // Here you should write your time consuming task...
                    // Let the progress ring for 10 seconds...
                    Thread.sleep(10000);
                } catch (Exception e) {

                }
                ringProgressDialog.dismiss();
            }
        }).start();
    }

    public void alertOrderMessage(final String param, final String orderId) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE: // Yes button clicked
                        if (param.equals("apply")) {
                            //muhdialog = ProgressDialog.show(MainActivity.this, "","Loading. Menunggu persetujuan dari Server", true);

                            Intent i = new Intent(getApplicationContext(), ExecuteActivity.class);
                            i.putExtra("orderMessage", notificationMessage);
                            startActivity(i);
                            finish();

                            mApplyOrderTask.execute(null, null, null);

                        } else if (param.equals("decline")) {

                        }
                        //Toast.makeText(MainActivity.this, "Yes Clicked "+orderId, Toast.LENGTH_LONG).show();
                        break;
                   case DialogInterface.BUTTON_NEGATIVE: // No button clicked // do nothing
                        if (param.equals("apply")) {

                        } else if (param.equals("decline")) {

                        }
                        //Toast.makeText(MainActivity.this, "No Clicked "+orderId, Toast.LENGTH_LONG).show();
                        break;
                 }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure?")
                .setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener)
                .show();
    }



}



/*public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}*/
