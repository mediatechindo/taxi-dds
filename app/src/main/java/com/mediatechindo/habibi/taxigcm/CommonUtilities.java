package com.mediatechindo.habibi.taxigcm;

/**
 * Created by habibi on 12/11/14.
 */
import android.content.Context;
import android.content.Intent;

public final class CommonUtilities {

    // give your server registration url here
    //LOCAL
    //static final String SERVER_URL = "http://192.168.1.111/taxi.lc/welcome/gcm_registration";
    //static final String SERVER_APPLYORDER_URL = "http://192.168.1.111/taxi.lc/welcome/apply_order";
    //static final String SERVER_UPDATE_LATLNG = "http://192.168.1.111/taxi.lc/welcome/update_latlng";

    // Online
    static final String SERVER_URL = "http://mediatechindo.com/dds/welcome/gcm_registration";
    static final String SERVER_APPLYORDER_URL = "http://mediatechindo.com/dds/mobl/apply_order";
    //static final String SERVER_UPDATE_LATLNG = "http://mediatechindo.com/dds/welcome/update_latlng";
    static final String SERVER_UPDATE_LATLNG = "http://mediatechindo.com/dds/mobl/update_latlng";
    static final String SERVER_URL_LOGIN = "http://mediatechindo.com/dds/welcome/login";
    //static final String SERVER_URL_DRIVER_CANCEL = "http://mediatechindo.com/dds/welcome/driver_cancel";

    static final String SERVER_URL_CHECK_PHONE = "http://mediatechindo.com/dds/mobl/phone";
    static final String SERVER_URL_UPDATE_REGID = "http://mediatechindo.com/dds/mobl/update_regid";
    static final String SERVER_URL_DRIVER_CANCEL = "http://mediatechindo.com/dds/mobl/driver_cancel";

    // Google project id
    static final String SENDER_ID = "271235343915";

    /**
     * Tag used on log messages.
     */
    static final String TAG = "Taxi GCM App";

    static final String DISPLAY_MESSAGE_ACTION =
            "com.mediatechindo.habibi.taxigcm.DISPLAY_MESSAGE";

    public static final String ACTION_PULSE_SERVER_ALARM =
            "com.mediatechindo.habibi.ACTION_PULSE_SERVER_ALARM";

    static final String EXTRA_MESSAGE = "message";
    static final String EXTRA_MESSAGE_TYPE = "messagetype";

    public static final int GPS_DELAY_UPDATE = 240; //4 minutes

    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    static void displayMessage(Context context, String message, String messageType) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(EXTRA_MESSAGE_TYPE, messageType);
        context.sendBroadcast(intent);
    }
}
